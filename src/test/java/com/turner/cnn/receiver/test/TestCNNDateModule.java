package com.turner.cnn.receiver.test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.jms.JMSException;

import org.junit.Test;

import com.turner.cnn.receiver.plugin.CNNDateModule;
import com.turner.loki.GenericMessage;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;

/**
 * Make sure the CNN Date Module takes in a date and 2 parameters for above the date
 * and below the date. Assert that the plugin outputs each date from the lowest to the
 * highest date as a parameter in the message called "scheduleDate".
 * @author Matt Gleason
 *
 */
public class TestCNNDateModule {

	@Test
	public void testDateDiff() throws JMSException, PluginException, InitializationException {
		CNNDateModule dateModule = new CNNDateModule();
		
		Hashtable<String, String> table = new Hashtable<String, String>();
		table.put("plusDays", "10");
		table.put("minusDays", "10");
		
		dateModule.initialize(table);
		
		GenericMessage m = new GenericMessage();
		m.setStringProperty("scheduleDate", "2014-01-17");
		
		ArrayList<GenericMessage> list = dateModule.multiplex(m);
		List<String> datesList = new ArrayList<String>();
		
		for (GenericMessage message : list) {
			datesList.add(message.getStringProperty("scheduleDate"));
		}
		
		for (int x = 7; x <= 27; x++) {
			if (x < 10)
				assertThat(datesList.contains("2014-01-0" + x), is(true));
			else
				assertThat(datesList.contains("2014-01-" + x), is(true));
		}
	}
}
