package com.turner.cnn.receiver.plugin;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;

import javax.jms.Message;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

import com.cnnsi.util.object.JdbcHelper;
import com.cnnsi.util.object.pooling.JdbcConnectionPool;
import com.turner.loki.AbstractPlugin;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;

/**
 * User: Bryan Korff Date: 4/26/13 Time: 10:30 AM
 */
public abstract class DataUpdater extends AbstractPlugin {
	/**
	 * DataUpdater is a base class derived by receiver plugins that write to the
	 * database. It has helper functions to derive the input message, parse the
	 * XML of the input message, get a database connection, release it and to
	 * convert null columns to empty strings.
	 * 
	 */

	public org.apache.log4j.Logger logger = Logger.getLogger(this.getClass());

	public String classReference = this.getClass().toString();
	private JdbcConnectionPool jcp = null;

	protected String cacheName = null;

	public DataUpdater() {
		classReference = classReference.substring(
				classReference.lastIndexOf(".") + 1, classReference.length());
	}

	GenericMessage getGM(Message m) throws PluginException {
		try {
			if (m instanceof GenericMessage) {
				return (GenericMessage) m;
			} else {
				return GenericMessageFactory.createGenericMessage(m);
			}
		} catch (Exception e) {
			throw new PluginException("Could not create GenericMessage", e);
		}
	}

	String setCompetitionName(Message gm) throws PluginException {
		String competitionName = null;

		try {
			if (gm.getStringProperty("seasonStage") != null){
				String seasonStage		= gm.getStringProperty("seasonStage");
				String sportsTypeUpper	= gm.getStringProperty("sportsType").toUpperCase();

				if (seasonStage.equalsIgnoreCase("PRE"))
					seasonStage = "_Pre";
				else if (seasonStage.equalsIgnoreCase("REG"))
					seasonStage = "_Reg";
				else if (seasonStage.equalsIgnoreCase("PST"))
					seasonStage = "_Post";
				else
					throw new PluginException("seasonStage, " + seasonStage + ", was not recognized.");

				competitionName = sportsTypeUpper + seasonStage;

				gm.setStringProperty("competitionName", competitionName);
			} else {
				throw new PluginException("seasonStage was not set.");
			}
		} catch (Exception e) {
			throw new PluginException("Could not set competitionName", e);
		}

		return competitionName;
	}

	Document getDocument(GenericMessage gm) throws PluginException {
		ByteArrayInputStream bais = null;

		try {
			byte body[] = gm.getBytesProperty("body");
			bais = new ByteArrayInputStream(body);
			return new SAXBuilder().build(bais);
		} catch (Exception e) {
			logger.error("Exception:  " + e.getMessage());

			throw new PluginException("Failed to parse message body XML.", e);
		} finally {
			if (bais != null)
				try {
					bais.close();
				} catch (Exception e) {
					logger.warn("Exception closing input stream for message body parsing.");
				}
		}
	}

	String getMD5Sum(GenericMessage gm) throws PluginException {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");

			md5.update(gm.getBytesProperty("body"));

			String md5sum = new BigInteger(1, md5.digest()).toString(16);

			while (md5sum.length() < 32) md5sum = "0" + md5sum;

			return md5sum;
		} catch(Exception e) {
			logger.error("Exception:  " + e.getMessage());

			throw new PluginException("Failed to parse message body XML.", e);
		}
	}

	String nullToEmptyString(String input) {
		if (input == null)
			return "";

		return input;
	}

	String makeQuotesSingle(String input) {
		return input.replaceAll("\"", "'");
	}

	public void closeStatement(Statement stmt) {
		if (stmt != null) try { stmt.close(); } catch (SQLException ignore) { logger.error(ignore); }
	}

	Connection getConnection() throws PluginException {
		try {
			return (Connection) jcp.acquire();
		} catch (Exception e) {
			logger.error("Exception:  " + e.getMessage());

			throw new PluginException(
					"Failed to acquire a connection from the connection pool.",
					e);
		}
	}

	void release(Connection conn, Statement stmt, ResultSet rs) {
		try {
			JdbcHelper.release(jcp, conn, stmt, rs);
		} catch (Exception e) {
			logger.warn("Exception closing input stream or jdbc resources.");
		}
	}

	public void initialize(Hashtable props) throws InitializationException {
		if (props.containsKey("poolName")) {
			String poolNamespace = (String) props.get("poolName");

			jcp = JdbcConnectionPool.getInstance(poolNamespace);
		} else {
			throw new InitializationException(
					"Missing required DataUpdater Batch Loader parameter [poolName].");
		}
	}

	public PreparedStatement prepareStatement(Connection conn, String sql)
			throws PluginException {
		try {
			logger.info("Prepare SQL " + sql);
			return conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, // .TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
		} catch (SQLException sql_e) {
			logger.error("Failed preparing statement for:  " + sql);

			throw new PluginException("Preparing statement failed:  "
					+ sql_e.getMessage());
		}
	}

	public String getNextVal(Connection conn, String query) throws SQLException {
		Statement stmt = null;

		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			if (rs.next()) {
				return rs.getString(1);
			} else {
				throw new SQLException(
						"Very strange; the nextVal query failed to find a record for:  "
								+ query);
			}
		} catch (SQLException sql_e) {
			logger.error("Getting nextval failed:  " + sql_e.getMessage());

			throw sql_e;
		} finally {
			closeStatement(stmt);
		}
	}

	public String getTeamId(Connection conn, String sdTeamId, String sport) throws PluginException {
		PreparedStatement   stmt   = null;
		String              teamId = null;

		try {
			stmt = conn.prepareStatement("SELECT TEAM_ID FROM TEAM WHERE SPORTS_DATA_TEAM_ID = ? AND SPORTS = ?");

			stmt.setString(1, sdTeamId);
			stmt.setString(2, sport);

			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				rs.getString("TEAM_ID");
				teamId = rs.getString("TEAM_ID");
			}

			rs.close();
		} catch (SQLException sql_e) {
			logger.error("fetching teamId failed:  " + sql_e.getMessage());

			throw new PluginException(sql_e);
		} finally {
			closeStatement(stmt);
		}

		return teamId;
	}

	public java.sql.Timestamp getCurrentDBTimestamp(Connection conn)
			throws SQLException {
		String timeQuery = "SELECT TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS') FROM DUAL";

		PreparedStatement stmt = null;

		try {
			stmt = conn.prepareStatement(timeQuery);

			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				return JdbcHelper.getSqlDate(rs.getString(1),
						"yyyy/MM/dd HH:mm:ss");
			} else {
				logger.error("Very strange; the timestamp query failed to find a record.");
			}

			rs.close();
		} catch (SQLException sql_e) {
			logger.error("inserting/updating player record failed:  "
					+ sql_e.getMessage());

			throw sql_e;
		} finally {
			closeStatement(stmt);
		}

		logger.warn("Failed to get current timestamp from database; using system time.");

		return new java.sql.Timestamp(System.currentTimeMillis());
	}

	GenericMessage createMessage(GenericMessage	gm,
								 String			nextAction,
								 String			sportsType,
								 String			msgType,
								 String			sportName,
								 String			seasonId,
								 String			seasonStage) throws Exception {
		GenericMessage message = GenericMessageFactory.createGenericMessage(gm);

		logger.debug("Populating msg:  " + nextAction + "|" +
										   sportsType + "|" +
										   msgType + "|" +
										   sportName + "|" +
										   seasonId + "|" +
										   seasonStage);

		message.setStringProperty("nextAction",		nextAction);
		message.setStringProperty("sportsType",		sportsType);
		message.setStringProperty("msgType",		msgType);
		message.setStringProperty("sportName",		sportName);
		message.setStringProperty("seasonId",		seasonId);
		message.setStringProperty("seasonStage",	seasonStage);

		setCompetitionName(message);

		return message;
	}
}
