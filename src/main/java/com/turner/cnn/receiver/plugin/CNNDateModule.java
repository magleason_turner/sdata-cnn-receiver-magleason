package com.turner.cnn.receiver.plugin;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import javax.jms.JMSException;
import javax.jms.Message;

import org.apache.log4j.Logger;

import com.turner.loki.AbstractPlugin;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.loki.core.interfaces.InitializableIF;
import com.turner.loki.core.interfaces.MultiplexIF;

/**
 * User: Matt Gleason 
 * Date: 1/16/2014 
 * Time: 12:44 PM
 * 
 * This class will be used to receive a message that will contain an optional
 * base date as well as an above value and below value representing the amount
 * of days above and below the base date to return. If no base date is given
 * then the current system date is taken.
 */
public class CNNDateModule extends AbstractPlugin implements MultiplexIF, InitializableIF{
	public int m_PlusDays;
	public int m_MinusDays;
	
	public void initialize(Hashtable properties) throws InitializationException{
		
		if(properties.keySet().contains("plusDays")) {
			m_PlusDays = Integer.parseInt((String) properties.get("plusDays"));
		}
			
		if(properties.keySet().contains("minusDays")) {
			m_MinusDays = Integer.parseInt((String) properties.get("minusDays"));
		}
	}
	
	public ArrayList<GenericMessage> multiplex(final Message m) throws PluginException {
		final Logger logger = Logger.getLogger(this.getClass());
		final ArrayList<GenericMessage> datesToSend = new ArrayList<GenericMessage>();
		final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date baseDate;
		
		String stringDate = null;
		try {
			stringDate = ((String) m.getStringProperty("scheduleDate"));
		} catch (JMSException e) {
			logger.error("Exception: " + e.getMessage());
			throw new PluginException("Failed to get baseDate property from message");
		}
		try {
			baseDate = dateFormat.parse(stringDate);
		} catch (ParseException e) {
			logger.error("Exception: " + e.getMessage());
			throw new PluginException("Failed to parse date");
		}
		
		//traverse each date from the minus date to the plus date
		//put each date in a message into the list of messages and return it
		final Date minusDate = getDateDiff(baseDate, -m_MinusDays);
		final Date plusDate = getDateDiff(baseDate, m_PlusDays);
		Date travDate = minusDate;
		
		while(!travDate.after(plusDate)) {
			GenericMessage message = new GenericMessage();
			try {
				message.setStringProperty("scheduleDate", dateFormat.format(travDate));
				datesToSend.add(message);
			} catch (JMSException e) {
				logger.error("Exception: " + e.getMessage());
				throw new PluginException("Failed to format date!");
			}
			travDate = new Date(travDate.getTime() + 1 * 24 * 3600 * 1000 );
		}
		
		return datesToSend;
	}
	
	/**
	 * Verify the message sent is of type GenericMessage otherwise throw an exception.
	 * @param m
	 * 		the message to verify
	 * @return
	 * 		the GenericMessage created from the message
	 * @throws PluginException
	 * 		thrown if the message is not of type GenericMessage
	 */
	GenericMessage getGM(Message m) throws PluginException {
		try {
			if (m instanceof GenericMessage) {
				return (GenericMessage) m;
			} else {
				return GenericMessageFactory.createGenericMessage(m);
			}
		} catch (Exception e) {
			throw new PluginException("Could not create GenericMessage", e);
		}
	}
	
	/**
	 * Get the date calculated from the baseDate parameter then add the number
	 * of days to find the difference.
	 * @param baseDate
	 * 		the base date to use
	 * @param days
	 * 		the number of days to add to the base date
	 * @return
	 * 		the newly calculated date
	 */
	public Date getDateDiff(final Date baseDate, final int days) {
		return new Date(baseDate.getTime() + days * 24 * 3600 * 1000 );
	}
}
